#!/usr/bin/python3

# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

if __name__ == "__main__":
    setup(
        name="pubpaste",
        author="The Anarcat",
        author_email="anarcat@debian.org",
        url="https://gitlab.com/anarcat/pubpaste/",
        python_requires=">=3.6",
        long_description=long_description,
        long_description_content_type="text/markdown",
        use_scm_version=True,
        classifiers=[
            "Development Status :: 4 - Beta",
            "Environment :: X11 Applications",
            "Intended Audience :: Developers",
            "Intended Audience :: System Administrators",
            "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",  # noqa: E501
            "Operating System :: POSIX",
            "Programming Language :: Python :: 3",
            "Topic :: Communications :: File Sharing",
        ],
        setup_requires=['setuptools_scm'],
        entry_points={
            'console_scripts': [
                'pubpaste=pubpaste:main',
            ],
        },
        project_urls={  # Optional
            'Bug Reports': 'https://gitlab.com/anarcat/pubpaste/issues',
            'Say Thanks!': 'http://saythanks.io/to/anarcat',
        },
    )
