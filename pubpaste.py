#!/usr/bin/python3
# coding: utf-8

"""publish files on the internet"""

# This is a (partial) rewrite of weasel's "publish" program. See the
# `parse_args` function for details on the behavior changes and
# variations.

# Copyright (C) 2020 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
import atexit
from datetime import datetime, timedelta, timezone
import logging
import os.path
from pathlib import Path
import secrets  # python 3.6
import shutil
import subprocess
import sys
import tempfile
from urllib.parse import quote
import yaml

try:
    from shlex import join as shlex_join
except ImportError:
    import shlex

    # copied almost verbatim from python 3.8
    def shlex_join(split_command):
        return " ".join(shlex.quote(arg) for arg in split_command)


__epilog__ = """ This program will upload the provided files on the internet and
notify the user when completed. The resulting URL will also be copied
to the clipboard. Preprocessors can do fancy things before (only
supports building an image gallery with sigal for now)."""


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized befure this, using
    `basicConfig`.
    """

    def __init__(self, *args, **kwargs):
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)


class ConfigAction(argparse.Action):
    """add configuration file to current defaults.

    a *list* of default config files can be specified and will be
    parsed when added by ConfigArgumentParser.
    """

    def __init__(self, *args, **kwargs):
        """the config action is a search path, so a list, so one or more argument"""
        kwargs["nargs"] = 1
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):
        """change defaults for the namespace, still allows overriding
        from commandline options"""
        for path in values:
            config = self.parse_config(path)
            if config:
                parser.set_defaults(**config)

    def parse_config(self, path):
        """abstract implementation of config file parsing, should be overriden in subclasses"""
        raise NotImplementedError()


class YamlConfigAction(ConfigAction):
    """YAML config file parser action"""

    def parse_config(self, path):
        try:
            with open(os.path.expanduser(path), "r") as handle:
                logging.debug("parsing path %s as YAML" % path)
                return yaml.safe_load(handle)
        except (FileNotFoundError, yaml.parser.ParserError) as e:
            raise argparse.ArgumentError(self, e)


class ConfigArgumentParser(argparse.ArgumentParser):
    """argument parser which supports parsing extra config files

    Config files specified on the commandline through the
    YamlConfigAction arguments modify the default values on the
    spot. If a default is specified when adding an argument, it also
    gets immediately loaded.

    This will typically be used in a subclass, like this:

        self.add_argument('--config', action=YamlConfigAction, default=self.default_config())

    """

    def _add_action(self, action):
        # this overrides the add_argument() routine, which is where
        # actions get registered. it is done so we can properly load
        # the default config file before the action actually gets
        # fired. Ideally, we'd load the default config only if the
        # action *never* gets fired (but still setting defaults for
        # the namespace) but argparse doesn't give us that opportunity
        # (and even if it would, it wouldn't retroactively change the
        # Namespace object in parse_args() so it wouldn't work).
        action = super()._add_action(action)
        if isinstance(action, ConfigAction) and action.default is not None:
            # fire the action, later calls can override defaults
            try:
                action(self, None, action.default, None)
                logging.debug("loaded config file: %s" % action.default)
            except argparse.ArgumentError as e:
                # ignore errors from missing default
                logging.debug("default config file %s error: %s" % (action.default, e))

    def default_config(self):
        """handy shortcut to detect commonly used config paths"""
        return [
            os.path.join(
                os.environ.get("XDG_CONFIG_HOME", "~/.config/"),
                os.path.basename(self.prog) + ".yml",
            )
        ]


class PubpasteArgumentParser(ConfigArgumentParser):
    def __init__(self, *args, **kwargs):
        """
        override constructor to setup our arguments and config file
        """
        super().__init__(description=__doc__, epilog=__epilog__, *args, **kwargs)
        # This is the usage of weasel's publish:
        #
        # usage: publish [<src> [<src> ...]]
        #
        # copy the file <src> to a server and report the URL.
        #
        # OPTIONS:
        #    -8        Add a AddDefaultCharset UTF-8 .htaccess file.
        #    -c CF     Use config file CF.
        #    -H        Show the history.
        #    -l        Show last used token.
        #    -s FN     When reading data from stdin, use FN as filename to be published.
        #    -S        Make a screenshot of one window and publish.
        #    -h        Show this message.
        #    -n        no-do.  Just print what would have been done.
        #    -q        Produce a QR code.
        #    -r        Add --relative option to rsync so that path names of the given
        #              files are preserved at the remote host.
        #    -t days   time to live in days
        #    -R        re-publish (re-use last directory token)
        #    -T tag    directory name on the server (use this to re-publish under that name)
        #    -x        Publish the contents of the xclipboard.
        #    -u        Unpublish directory (only useful together with -T)
        #    -L        Follow symlinks
        self.add_argument(
            "-v",
            "--verbose",
            action=LoggingAction,
            const="INFO",
            help="enable verbose messages",
        )
        self.add_argument(
            "-d",
            "--debug",
            action=LoggingAction,
            const="DEBUG",
            help="enable debugging messages",
        )
        self.add_argument("--dryrun", "-n", action="store_true", help="do nothing")

        group = self.add_argument_group("history arguments")
        group.add_argument(
            "-t",
            "--ttl",
            help="how long to keep the entry, in days, default: %(default)s",
        )
        group = group.add_mutually_exclusive_group()
        group.add_argument("-T", "--token", help="secret token, default: generated")
        group.add_argument(
            "-R", "--republish", action="store_true", help="reuse previous secret token"
        )

        self.add_argument(
            "--follow-symlinks", "-L", action="store_true", help="follow symlinks"
        )
        self.add_argument(
            "-s",
            "--stdin-name",
            metavar="NAME",
            default="stdin.txt",
            help="use NAME as filename when reading from stdin, default: %(default)s",
        )
        self.add_argument(
            "-o",
            "--output",
            help=argparse.SUPPRESS,  # only in configuration file
            default=None,
        )
        self.add_argument(
            "--url-prefix",
            help=argparse.SUPPRESS,  # only in configuration file
            default=None,
        )
        self.add_argument(
            "--base-dir",
            help=argparse.SUPPRESS,  # only in configuration file
            default=None,
        )
        self.add_argument(
            "--save-screenshots",
            help=argparse.SUPPRESS,  # only in configuration file
        )
        self.add_argument(
            "--select",
            action="store_true",
            help="tell screenshot tool to let the user select an area, default: fullscreen",
        )
        commands_group = self.add_argument_group(
            "command arguments", description="what should be done, default: just upload"
        )
        group = commands_group.add_mutually_exclusive_group()
        group.add_argument(
            "-x",
            "--xselection",
            action="store_const",
            const="xselection",
            dest="command",
            help="publish the contents of the X selection",
        )
        group.add_argument(
            "-S",
            "--screenshot",
            action="store_const",
            const="screenshot",
            dest="command",
            help="capture and upload a screenshot",
        )
        group.add_argument(
            "-g",
            "--gallery",
            action="store_const",
            const="gallery",
            dest="command",
            help="make a static image Sigal gallery from the files",
        )
        group.add_argument(
            "-l",
            "--last-token",
            action="store_const",
            const="last-token",
            dest="command",
            help="show the last token used and exit",
        )
        group.add_argument(
            "-H",
            "--show-history",
            action="store_const",
            const="show-history",
            dest="command",
            help="dump history file and exit",
        )
        group.add_argument(
            "-u",
            "--undo",
            action="store_const",
            const="undo",
            dest="command",
            help="delete uploaded token specified with -T or -R and exit",
        )
        group.add_argument(
            "-P",
            "--purge",
            action="store_const",
            const="purge",
            dest="command",
            help="purge old entries from remote server",
        )
        group.add_argument("--command", default="upload", help=argparse.SUPPRESS)

        self.add_argument(
            "--config", action=YamlConfigAction, default=self.default_config()
        )
        self.add_argument("files", nargs="*", help="files to upload")

    def parse_args(self, *args, **kwargs):
        res = super().parse_args(*args, **kwargs)
        if res.output and not res.output.endswith("/"):
            res.output += "/"
        if res.url_prefix and not res.url_prefix.endswith("/"):
            res.url_prefix += "/"
        if res.dryrun:
            if not res.files:
                self.error("cannot read from stdin in dryrun")
            if res.command == "xselection":
                self.error("cannot read from x selection in dryrun")
        if res.files and res.command and res.command != "gallery":
            self.error(
                "command %s and files cannot be specified together" % res.command
            )
        if res.command and res.command == "purge" and args.base_dir is None:
            self.error("no --base-dir specified, cannot find files to purge")

        return res


def secret_token():
    """return a secret token made of multiple components, as a tuple"""
    return datetime.now().strftime("%Y-%m-%d"), secrets.token_urlsafe()


class Processor(object):
    def __init__(self, args, dryrun=False):
        self.args = args
        self.dryrun = dryrun

    def process(self, paths, tmp_dir):
        """process the given paths

        Returns the modified paths, or None if not modified. A tmp_dir
        is provided by the caller and automatically destroyed.
        """
        raise NotImplementedError()


class Sigal(Processor):
    """the Sigal processor will create a temporary gallery in the provided
    tmp_dir, after copying the provided files in the said
    directory. it returns only the "build" directory.
    """

    def process(self, paths, tmp_dir):
        output_dir = Path(tmp_dir) / Path(self.args.token)
        if not output_dir.exists() and not self.dryrun:
            logging.info("creating directory %s", output_dir)
            output_dir.mkdir(parents=True)

        pictures_dir = output_dir / "pictures"
        if not pictures_dir.exists() and not self.dryrun:
            logging.info("creating directory %s", pictures_dir)
            pictures_dir.mkdir(parents=True)

        conf_file = output_dir / "sigal.conf.py"
        if not conf_file.exists() and not self.dryrun:
            logging.info("creating config file %s", output_dir)
            with conf_file.open("w") as c:
                c.write(self.sigal_minimal_config())

        for path in paths:
            logging.info("copying %s into %s", path, pictures_dir)
            if not self.dryrun:
                shutil.copy2(
                    path, str(pictures_dir), follow_symlinks=self.args.follow_symlinks
                )

        build_dir = output_dir / "_build"
        command = (
            "sigal",
            "build",
            "--config",
            str(conf_file),
            str(pictures_dir),
            str(build_dir),
        )
        logging.info("building gallery with %r", shlex_join(command))
        if not self.dryrun:
            subprocess.check_call(command)
        return (str(build_dir),)

    @classmethod
    def sigal_minimal_config(cls):
        """a string representing a good minimal sigal configuration for our
use case.

        sigal default settings are generally great, but i disagree on
        those.

        .. TODO:: allow users to provide their own config
        """
        return """
# theme: colorbox (default), galleria, photoswipe, or path to custom
theme = 'galleria'
# sort files by date (default: filename)
medias_sort_attr = 'date'
# "Standard HD", or 720p (default: (640, 480))
img_size = (1280, 720)
# "Standard HD", or 720p (default: (480, 360))
video_size = (1280, 720)
# skip first three seconds in video (default: 0)
thumb_video_delay = '3'
"""


class Maim(Processor):
    """The Main processor will take a screenshot and return the file after
    copying it to the temporary directory"""

    def __init__(self, args, dryrun=False, save_screenshots=False):
        super().__init__(args, dryrun=dryrun)
        if args.select:
            self.command = "maim --select --delay=3 '%s'"
        else:
            self.command = "maim --delay=3 '%s'"
        self.save_screenshots = save_screenshots

    def process(self, paths, tmp_dir):
        """wrap main around a timer, preview and prompt"""
        if self.save_screenshots:
            snaps_dir = Path(os.path.expanduser(self.save_screenshots))
            if not snaps_dir.exists():
                snaps_dir.mkdir()
        else:
            snaps_dir = Path(tmp_dir)
        snap_basename = Path(
            "snap-" + datetime.now().strftime("%Y%m%dT%H%M%S%Z") + ".png"
        )
        snap_path = snaps_dir / snap_basename
        # XXX: this is just horrible. can't the screenshot tool do the right thing here?
        #
        # TODO: at the very least, make our own popup, because right
        # now this doesn't catch errors from maim, as errors from the
        # subprocess are not trickled back up through xterm
        command = (
            "xterm",
            "-title",
            "pop-up",
            "-geometry",
            "80x3+5+5",
            "-e",
            self.command % snap_path,
        )
        logging.debug("running screenshot with %s", shlex_join(command))
        if not args.dryrun:
            success = subprocess.call(command) == 0
            if not success:
                notify_user("screenshot command failed, aborting")
                return []
        if not snap_path.exists():
            notify_user("snapshot file missing, is maim installed?")
            return []
        if snap_path.stat().st_size <= 0:
            notify_user("snapshot file is empty, aborting")
            snap_path.unlink()
            return []
        # we delegate the customization of this to XDG
        subprocess.Popen(("xdg-open", str(snap_path)))  # nowait
        # XXX: gah. we *could* just do GTK here?
        command = (
            "xmessage",
            "-buttons",
            "okay:0,cancel:1",
            "-center",
            "-default",
            "okay",
            "upload snapshot publically?",
        )
        try:
            retcode = subprocess.call(command)
        except OSError as e:
            logging.error("cannot find xmessage? %s", e)
            return []
        if retcode == 0:
            target = str(Path(tmp_dir) / snap_basename)
            if not self.save_screenshots:
                logging.debug("returning %s", tmp_dir)
                return (target,)
            shutil.copy(str(snap_path), target)
            # pass the resulting file back to the uploader
            logging.info("screenshot copied from %s to %s", snap_path, target)
            return (target,)
        elif retcode == 1:
            logging.info("user declined, aborting")
        elif retcode > 1:
            logging.error("unexpected error from xmessage")
        elif retcode < 0:
            logging.error("xmessage terminated by signal %s", -retcode)
        return []


class Uploader(object):
    """an abstract class to wrap around upload objects"""

    def __init__(self, args, dryrun=False):
        self.args = args
        self.dryrun = dryrun

    def upload(self, paths, target, follow_symlinks=False, single=False):
        """upload the given path to the target

        "Single" is an indication by the caller of whether or not this
        is the only item in a list to upload.
        """
        raise NotImplementedError()


class RsyncUploader(Uploader):
    base_rsync_command = (
        "rsync",
        "--recursive",
        "--compress",
        "--times",
        "--chmod=u=rwX,go=rX",
    )

    def upload(self, path, target, follow_symlinks=False, single=False):
        """upload the given path to the target

        "Single" is an indication by the caller of whether or not this
        is the only item in a list to upload. The RsyncUploader uses
        that information to decide how many levels it should replicate
        remotely.
        """
        command = list(self.base_rsync_command)
        if follow_symlinks:
            command.append("--copy-links")  # -L
        # XXX: begin nasty rsync commandline logic

        # rsync is weird. it behaves differently when its arguments
        # have trailling slashes or not. this specifically affects
        # directory (but technically, if you pass a file with a
        # trailing slash, it will fail, obviously)
        #
        # the behavior is documented in the manpage, as such:

        # A trailing slash on the source changes this behavior to
        # avoid creating an additional directory level at the
        # destination. You can think of a trailing / on a source as
        # meaning "copy the contents of this directory" as opposed to
        # "copy the directory by name", but in both cases the
        # attributes of the containing directory are transferred to
        # the containing directory on the destination. In other words,
        # each of the following commands copies the files in the same
        # way, including their setting of the attributes of /dest/foo:
        #
        # rsync -av /src/foo /dest
        # rsync -av /src/foo/ /dest/foo

        # They ommitted, obviously, that this is also identical:
        #
        # rsync -av /src/foo/ /dest/foo/
        #
        # So we pick the latter form, IF WE UPLOAD A SINGLE DIRECTORY!
        # If we upload MULTIPLE FILES OR DIRECTORIES, we CANNOT use
        # the above form, as the last uploads would overwrite the
        # first ones. So if we have more than one file passed on the
        # commandline, we do, effectively, this:
        #
        # rsync -av /srv/foo /srv/bar /dest/foo/
        #
        # which, yes, is actually equivalent to:
        #
        # rsync -av /srv/foo /dest/foo/foo
        # rsync -av /srv/bar /dest/foo/bar

        # so here we go.

        # make sure we have a trailing slash at least to the
        # second argument, so we are *certain* we upload in a new
        # *directory*
        if not target.endswith("/"):
            target += "/"
        # for the source, special handling if it is a directory
        if os.path.isdir(path):
            if single:
                # single file to upload: upload at root which
                # means, for rsync, to have a trailing slash
                if not path.endswith("/"):
                    path += "/"
            else:
                # multiple files to upload: upload *within* the
                # root, so *remove* the trailing slash if present
                if path.endswith("/"):
                    path = path.rstrip("/")
        # XXX: end nasty rsync commandline logic
        command += (path, target)

        logging.debug("uploading with %r", shlex_join(command))
        if self.dryrun:
            return self.dryrun
        return subprocess.call(command) == 0

    def delete(self, target, tmp_dir):
        """delete the given target

        This is done by synchronizing it with the given directory, and
        by calling rsync with `--delete`. We also assert that the
        provided tmpdir is empty.
        """
        assert not list(
            Path(tmp_dir).iterdir()
        ), "tmpdir is not empty, delete would fail"
        command = list(self.base_rsync_command)
        command.append("--delete")
        command += (tmp_dir + "/", target + "/")
        logging.debug("deleting with %r", shlex_join(command))
        if self.dryrun:
            return self.dryrun
        return subprocess.check_call(command) == 0


def xselection_to_file(path):
    with open(path, "w+b") as tmp:
        exceptions = []
        for tool in ("xclip", "xsel"):
            command = (tool, "-o")
            try:
                clipboard = subprocess.check_output(command)
            except (FileNotFoundError, subprocess.CalledProcessError) as e:
                exceptions.append(e)
        if exceptions:
            logging.error("could not find tool to paste clipboard: %s", exceptions)
            return []
        # NOTE: could this be optimized? can we *stream* the
        # clipboard?
        tmp.write(clipboard)
    logging.info("written %d bytes from clipboard into %s", len(clipboard), path)
    return (path,)


history_path = os.path.expanduser("~/.publish.history")
TTL_PATH = ".publish.ttl"


def history_append(token, uri):
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S%Z")
    with open(history_path, "a") as fp:
        fp.write(" ".join((timestamp, token, uri)) + "\n")


def history_remove(tokens=()):
    """remove the given tokens from history"""
    with open(history_path, "r") as old, open(history_path + ".new", "w") as new:
        for line in old.readlines():
            date, time, token, uri = line.split(" ")
            if token not in tokens:
                new.write(line)
    os.rename(history_path + ".new", history_path)


def history_purge(base_dir):
    logging.debug("purging from %s", base_dir)
    for path in [p for p in Path(base_dir).iterdir() if p.is_dir()]:
        token = path.name
        ttl_file = path / TTL_PATH
        if not ttl_file.exists():
            logging.info("no TTL file, token % never expires", token)
            continue
        with ttl_file.open("r") as fp:
            ttl_str = fp.read()
        try:
            ttl = int(ttl_str)
        except ValueError as e:
            logging.warning(
                "invalid TTL found for token %s: %s in %s", token, e, ttl_file
            )
            continue
        now_utc = datetime.now(timezone.utc)
        local_tz = now_utc.astimezone().tzinfo
        last_modified = datetime.fromtimestamp(ttl_file.stat().st_mtime, tz=local_tz)
        diff = timedelta(days=ttl)
        logging.debug(
            "delta computation, last: %s, diff: %s days, last + diff: %s, delta: %s, now: %s",
            last_modified,
            diff,
            last_modified + diff,
            now_utc - last_modified + diff,
            now_utc,
        )
        if last_modified + diff < now_utc:
            notify_user(
                "token %s expired since %s, removing directory: %s"
                % (token, -(diff - (now_utc - last_modified)), path)
            )
            shutil.rmtree(path)
        else:
            logging.info(
                "TTL %s days not passed yet (%s left) for token %s",
                ttl,
                diff - (now_utc - last_modified),
                token,
            )


def notify_user(message):
    logging.warning(message)
    if os.environ.get("DISPLAY") is None:
        return
    command = ("notify-send", message)
    # do not fail on notifications
    if subprocess.call(command) != 0:
        logging.warning("failed to run command %r", command)


def main(args):
    if args.republish or args.command in ("last-token", "show-history", "undo"):
        with open(history_path, "r") as fp:
            for line in fp.readlines():
                if args.command == "show-history":
                    print(line, end="")
        date, time, token, uri = line.split(" ")
        logging.debug("found last entry: %s, %s, %s, %s", date, time, token, uri)
        # do not override provided token in undo, but assume republish otherwise
        if args.command == "undo" or not args.token:
            args.token = token
        if args.command == "show-history":
            sys.exit(0)
        if args.command == "last-token":
            print(args.token)
            sys.exit(0)
    if args.token is None:
        args.token = "-".join(secret_token())
    logging.debug("using secret token %s", args.token)

    if args.url_prefix:
        assert args.url_prefix.endswith(
            "/"
        ), "args parsing should have ensured a trailing slash"
        uri_with_token = args.url_prefix + args.token + "/"
    else:
        uri_with_token = None

    # files that we are confident we can show the user on the
    # terminal. those are typically stdin and xselection but could
    # grow to include text files..
    dump_content = []

    # list of cleanup functions to run after upload
    if not args.dryrun:
        tmp_dir = tempfile.TemporaryDirectory()
        stdin_tmp_path = tmp_dir.name + "/" + args.stdin_name
        atexit.register(tmp_dir.cleanup)

    if args.command == "undo":
        uploader = RsyncUploader(args, dryrun=args.dryrun)
        if not uploader.delete(args.output + args.token, tmp_dir.name):
            notify_user("failed to delete target %s" % args.token)
            sys.exit(1)
        history_remove(args.token)
        notify_user("deleted target %s" % args.token)
        sys.exit(0)

    elif args.command == "purge":
        history_purge(args.base_dir)
        sys.exit(0)

    elif args.command == "xselection":
        logging.info("reading from xselection...")
        args.files = xselection_to_file(stdin_tmp_path)
        dump_content = [stdin_tmp_path]

    elif args.command == "screenshot":
        m = Maim(args, dryrun=args.dryrun, save_screenshots=args.save_screenshots)
        args.files = m.process([], tmp_dir.name)

    elif args.command == "gallery":
        logging.debug("processing files %s with Sigal", args.files)
        f = Sigal(args, dryrun=args.dryrun).process(args.files, tmp_dir.name)
        if f is not None:
            args.files = f
            logging.debug("modified files: %s", args.files)

    elif '-' in args.files or not args.files:
        logging.info("reading from stdin...")
        with open(stdin_tmp_path, "w+b") as tmp:
            shutil.copyfileobj(sys.stdin.buffer, tmp)
        if '-' in args.files:
            for k, v in enumerate(args.files):
                if v == '-':
                    args.files[k] = stdin_tmp_path
        else:
            args.files = (stdin_tmp_path,)
        dump_content = [stdin_tmp_path]

    if not args.output:
        logging.error("no output provided, nothing to do, aborting")
        sys.exit(1)
    if not args.files:
        logging.error("no files provided, nothing to do, aborting")
        sys.exit(1)

    uploader = RsyncUploader(args)
    if logging.getLogger("").level >= 20:  # INFO or DEBUG
        uploader.base_rsync_command += ("--progress", "--verbose")
    target = args.output + args.token
    for path in args.files:
        # find the URI
        if uri_with_token:
            uri = uri_with_token
            if not os.path.isdir(path):
                uri += quote(path)
            logging.info("uploading %s to %s", path, uri)
        else:
            logging.info("uploading %s", path)
        if path in dump_content:
            with open(path) as fp:
                print("uploaded content: %r" % fp.read())
        assert args.output.endswith("/")

        if not uploader.upload(
            path,
            target=target,
            follow_symlinks=args.follow_symlinks,
            single=(len(args.files) == 1),
        ):
            notify_user("failed to upload %s as %s, aborting" % (path, args.token))
            sys.exit(1)

    if args.ttl:
        ttl_file = tmp_dir.name + "/" + TTL_PATH
        if not args.dryrun:
            with open(ttl_file, "w") as fp:
                fp.write(args.ttl + "\n")
        if not uploader.upload(ttl_file, target):
            notify_user("failed to upload TTL file for %s, but upload succeeded", args.token)

    history_append(args.token, uri_with_token or None)

    if not uri_with_token:
        notify_user("uploaded %s" % shlex_join(args.files))
        return

    uri = uri_with_token
    if len(args.files) == 1:
        path = Path(args.files[0])
        if not path.is_dir():
            uri += quote(os.path.basename(path))

    # NOTE: we use xclip here as the alternative is pyperclip, which
    # does exactly the same thing: https://pypi.org/project/pyperclip/
    if not os.environ.get("DISPLAY"):
        pasted = False
    else:
        command = ("xclip", "-selection", "clipboard")
        p = subprocess.Popen(command, stdin=subprocess.PIPE)
        p.communicate(uri.encode("utf-8"))
        pasted = p.returncode == 0
        if not pasted:
            logging.warning("could not copy to clipboard with: %r", command)

    message = "uploaded %s to '%s'" % (shlex_join(args.files), uri)
    if pasted:
        message += " copied to clipboard"
    if args.ttl:
        message += ", expiring in %s days" % args.ttl
    else:
        message += ", never expiring"
    notify_user(message)


if __name__ == "__main__":
    logging.basicConfig(format="%(message)s", level="WARNING")
    args = PubpasteArgumentParser().parse_args()
    main(args)
